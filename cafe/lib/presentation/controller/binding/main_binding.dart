import 'package:cafe/presentation/controller/home/main_controller.dart';
import 'package:get/instance_manager.dart';

class MainBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => MainController());
  }
}
