import 'package:cafe/presentation/controller/authenticate/sigin_controller.dart';
import 'package:get/instance_manager.dart';

class SiginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SignInController>(() => SignInController());
  }
}
