import 'package:cafe/domain/entities/food.dart';
import 'package:cafe/presentation/const/firebase_const.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/state_manager.dart';

class HomeController extends GetxController {
  RxList<Food> ls = RxList();
  List<Food> get lsFood => ls;

  void streamData() {
    firebaseFirestore
        .collection('categories')
        .get()
        .then((snapShot) => snapShot.docs.forEach((element) {
              print(element['id']);
            }));
  }

  Future getData() async {
    final list = await firebaseFirestore
        .collection('categories')
        .get()
        .then((value) => value.docs);
    for (var element in list) {
      firebaseFirestore
          .collection('categories')
          .doc(element.id)
          .collection('foods')
          .snapshots()
          .listen(_lestion);
    }
    for (var element in ls) {
      print(element);
    }
  }

  _lestion(QuerySnapshot<Map<String, dynamic>> snapshot) {
    var docs = snapshot.docs;
    for (var doc in docs) {
      Map<String, dynamic> data = doc.data();

      ls.add(Food.fromJson(data));
    }
  }
}
