import 'package:get/state_manager.dart';

class MainController extends GetxController {
  RxInt selectedIndex = 0.obs;
  onDestinationSelected(int index) => selectedIndex.value = index;
}
