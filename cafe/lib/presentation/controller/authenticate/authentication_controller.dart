import 'package:get/state_manager.dart';

class AuthenticationController extends GetxController {
  var isLoaded = true.obs;
  void onClicked() => isLoaded.value = !isLoaded.value;
}
