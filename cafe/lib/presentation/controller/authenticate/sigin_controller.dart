import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/datasource/local_storage.dart';
import '../../const/user.key.dart';

class SignInController extends GetxController {
  var check = true.obs;
  var visiblePass = false.obs;
  var visibleEmail = false.obs;
  var loading = false.obs;
  var conEmail = TextEditingController();
  var conPass = TextEditingController();
  void onChangeEmail(String value) =>
      visibleEmail.value = (value.isNotEmpty) ? true : false;
  void onChangePassword(String value) {
    if (value.isNotEmpty) {
      visiblePass.value = true;
    } else {
      visiblePass.value = false;
      check.value = true;
    }
  }

  void clearText() {
    conEmail.text = '';
    visibleEmail.value = !visibleEmail.value;
  }

  void showPassword() => check.value = !check.value;
  void isLoading(bool value) => loading.value = value;
  String? emailValidator(String? value) =>
      !EmailValidator.validate(value!) ? 'Email a valid' : null;
  Widget icon() =>
      Icon((check.isTrue) ? Icons.visibility : Icons.visibility_off);
  Future signIn(bool validate) async {
    if (validate) {
      isLoading(true);
      try {
        final respone = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: conEmail.value.text.trim(),
          password: conPass.value.text.trim(),
        );
        await _fetchData(respone.user!);
        isLoading(false);
      } on FirebaseAuthException catch (error) {
        isLoading(false);
        String str = 'Connection problem';
        if (error.code == 'user-not-found') {
          str = 'No user found for that email.';
        } else if (error.code == 'wrong-password') {
          str = 'ពាក្យសម្ងាត់មិនត្រឹមត្រូវ';
        }
        Get.defaultDialog(
          title: 'Error',
          content: Text(str),
        );
      }
    }
  }

  Future _fetchData(User currentUser) async {
    FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .get()
        .then((snapshot) {
      if (snapshot.exists) {
        Map<String, dynamic>? map = snapshot.data();
        LocalStorage.setString(key: uid, value: map![uid]);
        LocalStorage.setString(key: name, value: map[name]);
        LocalStorage.setString(key: email, value: map[email]);
        LocalStorage.setString(key: gender, value: map[gender]);
        LocalStorage.setString(key: date, value: map[date]);
        LocalStorage.setString(key: image, value: map[image]);
      }
      //LocalStorage.setString(key: uid, value: snapshot.);
    });
  }
}
