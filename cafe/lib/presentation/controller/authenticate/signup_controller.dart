import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as fstorage;
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import '../../../data/datasource/local_storage.dart';
import '../../../domain/entities/user.dart' as u;
import '../../const/user.key.dart' as k;
import '../../widget/alert.dart';

class SignUpController extends GetxController {
  final userName = TextEditingController().obs;
  final conPass = TextEditingController().obs;
  final conEmail = TextEditingController().obs;
  final conDatePicker = TextEditingController().obs;
  var pathImage = ''.obs;
  var checkPassword = true.obs;
  var visiblePassword = false.obs;
  var loading = false.obs;
  var gender = ['male', 'female'].obs;
  var userImage = ''.obs;
  var selectionGender = 'male'.obs;
  String? emailValidator(String? value) =>
      !EmailValidator.validate(value!) ? 'Email a valid' : null;
  void onChangePassword(String value) {
    if (value.isNotEmpty) {
      visiblePassword.value = true;
    } else {
      visiblePassword.value = false;
      checkPassword.value = true;
    }
  }

  void showPassword() => checkPassword.value = !checkPassword.value;
  void isLoading(bool value) => loading.value = value;
  Widget icon() =>
      Icon((checkPassword.isTrue) ? Icons.visibility : Icons.visibility_off);
  void onChangeGender(String? value) => selectionGender.value = value!;
  Widget circleAvatar(context) => CircleAvatar(
        radius: MediaQuery.of(context).size.width * 0.15,
        backgroundColor: Colors.black,
        child: CircleAvatar(
          radius: MediaQuery.of(context).size.width * 0.14,
          backgroundImage: (pathImage.value.isNotEmpty)
              ? FileImage(File(pathImage.value))
              : null,
          backgroundColor: Colors.white60,
          child: (pathImage.value.isNotEmpty)
              ? null
              : Icon(
                  Icons.person,
                  size: MediaQuery.of(context).size.width * 0.12,
                  color: Colors.white,
                ),
        ),
      );

  Future pickerImage({required ImageSource imageSource}) async {
    try {
      final image = await ImagePicker().pickImage(source: imageSource);
      if (image == null) return;
      pathImage.value = (await _cropImage(path: image.path))!;
      Get.back();
    } catch (e) {
      debugPrint('Pick image ${e.toString()}');
    }
  }

  Future<String?> _cropImage({required String path}) async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(sourcePath: path);
    return (croppedFile != null) ? croppedFile.path : null;
  }

  Future signUp({required bool validate, context}) async {
    if (validate) {
      isLoading(true);
      String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      fstorage.Reference reference = fstorage.FirebaseStorage.instance
          .ref()
          .child('users')
          .child(fileName);
      fstorage.UploadTask uploadTask = reference.putFile(File(pathImage.value));
      fstorage.TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() {});
      await taskSnapshot.ref.getDownloadURL().then((url) async {
        pathImage.value = url;
        //save info fireStore
        try {
          final respone =
              await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: conEmail.value.text.trim(),
            password: conPass.value.text.trim(),
          );
          await _register(respone.user!);
          isLoading(false);
        } on FirebaseAuthException catch (e) {
          isLoading(false);
          showDialog(
            context: context,
            builder: (context) => Alert(
                title: 'Authentication',
                content: e.toString(),
                onPreesed: () => Navigator.of(context).pop()),
          );
        }
      });
    }
  }

  Future _register(User currentUser) async {
    u.User user = u.User(
      uid: currentUser.uid,
      name: userName.value.text.trim(),
      email: currentUser.email!,
      gender: selectionGender.value,
      date: conDatePicker.value.text,
      img: pathImage.value,
      status: 1,
    );
    FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .set(user.toMap())
        .then((snapshot) {
      //local storage

      LocalStorage.setString(key: k.uid, value: currentUser.uid);
      LocalStorage.setString(key: k.name, value: userName.value.text);
      LocalStorage.setString(key: k.email, value: currentUser.email!);
      LocalStorage.setString(key: k.gender, value: selectionGender.value);
      LocalStorage.setString(key: k.date, value: conDatePicker.value.text);
      LocalStorage.setString(key: k.image, value: pathImage.value);
    });
  }
}
