import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as fstorage;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'package:cafe/presentation/const/firebase_const.dart';
import 'package:cafe/presentation/pages/authenticate/authentication.dart';
import 'package:cafe/presentation/pages/home/page.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../const/user.key.dart' as k;
import '../../domain/entities/user.dart' as u;

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  RxBool isLognIn = false.obs;
  late Rx<User?> userFirebase;
  final GlobalKey<FormState> keySignIn = GlobalKey<FormState>();
  final GlobalKey<FormState> keySignUp = GlobalKey<FormState>();
  final conUserName = TextEditingController();
  final conPass = TextEditingController();
  final conEmail = TextEditingController();
  final conDatePicker = TextEditingController();
  var pathImage = ''.obs;
  var checkPassword = true.obs;
  var visiblePassword = false.obs;
  var loading = false.obs;
  var gender = ['male', 'female'].obs;
  var userImage = ''.obs;
  var selectionGender = 'male'.obs;
  @override
  void onReady() {
    super.onReady();
    userFirebase = Rx<User?>(auth.currentUser);
    userFirebase.bindStream(auth.userChanges());
    ever(userFirebase, _setInitialScreen);
  }

  _setInitialScreen(User? user) {
    if (user != null) {
      // user is logged in
      Get.offAll(() => MainPage());
    } else {
      // user is null as in user is not available or not logged in
      Get.offAll(() => Authentication());
    }
  }

  void isLoading(bool value) => loading.value = value;

// void onChangeEmail(String value) =>
//       visibleEmail.value = (value.isNotEmpty) ? true : false;
  void onChangePassword(String value) {
    if (value.isNotEmpty) {
      visiblePassword.value = true;
    } else {
      visiblePassword.value = false;
      checkPassword.value = true;
    }
  }

  // void clearText() {
  //   conEmail.text = '';
  //   visibleEmail.value = !visibleEmail.value;
  // }
  Future signIn() async {
    if (keySignIn.currentState!.validate()) {
      isLoading(true);
      try {
        final respone = await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: conEmail.value.text.trim(),
          password: conPass.value.text.trim(),
        );
        await _fetchData(respone.user!);
        isLoading(false);
      } on FirebaseAuthException catch (error) {
        isLoading(false);
        String str = 'Connection problem';
        if (error.code == 'user-not-found') {
          str = 'No user found for that email.';
        } else if (error.code == 'wrong-password') {
          str = 'ពាក្យសម្ងាត់មិនត្រឹមត្រូវ';
        }
        Get.defaultDialog(
          title: 'Error',
          content: Text(str),
        );
      }
    }
  }

  signUp() async {
    if (keySignUp.currentState!.validate()) {
      isLoading(true);
      String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      fstorage.Reference reference = fstorage.FirebaseStorage.instance
          .ref()
          .child('users')
          .child(fileName);
      fstorage.UploadTask uploadTask = reference.putFile(File(pathImage.value));
      fstorage.TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() {});
      await taskSnapshot.ref.getDownloadURL().then((url) async {
        pathImage.value = url;
        //save info fireStore
        try {
          final respone =
              await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: conEmail.text.trim(),
            password: conPass.text.trim(),
          );
          await _register(respone.user!);
          isLoading(false);
        } on FirebaseAuthException {
          isLoading(false);
          Get.defaultDialog();
        }
      });
    }
  }

  signOut() {
    auth.signOut();
  }

  Future _register(User currentUser) async {
    u.User user = u.User(
      uid: currentUser.uid,
      name: conUserName.text.trim(),
      email: currentUser.email!,
      gender: selectionGender.value,
      date: conDatePicker.value.text,
      img: pathImage.value,
      status: 1,
    );
    FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .set(user.toMap())
        .then((snapshot) {
      //local storage
      GetStorage().write(k.uid, currentUser.uid);
      GetStorage().write(k.name, conUserName.text);
      GetStorage().write(k.email, currentUser.email);
      GetStorage().write(k.gender, selectionGender);
      GetStorage().write(k.date, conDatePicker.text);
      GetStorage().write(k.image, pathImage);
    });
  }

  Future _fetchData(User currentUser) async {
    FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .get()
        .then((snapshot) {
      if (snapshot.exists) {
        Map<String, dynamic>? map = snapshot.data();
        GetStorage().write(k.uid, map![k.uid]);
        GetStorage().write(k.name, map[k.name]);
        GetStorage().write(k.email, map[k.email]);
        GetStorage().write(k.gender, map[k.gender]);
        GetStorage().write(k.date, map[k.date]);
        GetStorage().write(k.image, map[k.image]);
      }
      //LocalStorage.setString(key: uid, value: snapshot.);
    });
  }

  String? emailValidator(String? value) =>
      // GetUtils.isEmail(value!) ? 'Email a valid' : null;
      !EmailValidator.validate(value!) ? 'Email a valid' : null;

  void showPassword() => checkPassword.value = !checkPassword.value;

  Widget icon() =>
      Icon((checkPassword.isTrue) ? Icons.visibility : Icons.visibility_off);
  void onChangeGender(String? value) => selectionGender.value = value!;
  Widget circleAvatar() => CircleAvatar(
        radius: Get.size.width * 0.15,
        backgroundColor: Colors.black,
        child: CircleAvatar(
          radius: Get.size.width * 0.14,
          backgroundImage: (pathImage.value.isNotEmpty)
              ? FileImage(File(pathImage.value))
              : null,
          backgroundColor: Colors.white60,
          child: (pathImage.value.isNotEmpty)
              ? null
              : Icon(
                  Icons.person,
                  size: Get.size.width * 0.12,
                  color: Colors.white,
                ),
        ),
      );

  Future pickerImage({required ImageSource imageSource}) async {
    try {
      final image = await ImagePicker().pickImage(source: imageSource);
      if (image == null) return;
      pathImage.value = (await _cropImage(path: image.path))!;
      Get.back();
    } catch (e) {
      debugPrint('Pick image ${e.toString()}');
    }
  }

  Future<String?> _cropImage({required String path}) async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(sourcePath: path);
    return (croppedFile != null) ? croppedFile.path : null;
  }
}
