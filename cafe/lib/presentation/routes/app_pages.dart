import 'package:cafe/presentation/controller/binding/main_binding.dart';
import 'package:cafe/presentation/pages/authenticate/authentication.dart';
import 'package:cafe/presentation/pages/home/page.dart';
import 'package:cafe/presentation/pages/splash.screen/splash_screen.dart';
import 'package:cafe/presentation/routes/app_routes.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

import '../controller/binding/authentication_binding.dart';

class AppPage {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.splashScreen,
      page: () => const SplashScreen(),
    ),
    GetPage(
      name: AppRoutes.mainPage,
      page: () => MainPage(),
      binding: MainBinding(),
    ),
    GetPage(
      name: AppRoutes.authentication,
      page: () => Authentication(),
      binding: AuthBinding(),
    ),
  ];
}
