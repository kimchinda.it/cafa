// // ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'package:flutter/material.dart';
// import 'package:sodacafe/presentation/widget/custom_icon.dart';
// import 'package:sodacafe/presentation/widget/food_card.dart';

// import '../../domain/entities/food.dart';

// class MenuFoodPage extends StatefulWidget {
//   final Future<List<Food>> ls;
//   const MenuFoodPage({
//     Key? key,
//     required this.ls,
//   }) : super(key: key);

//   @override
//   State<MenuFoodPage> createState() => _MenuFoodPageState();
// }

// class _MenuFoodPageState extends State<MenuFoodPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SingleChildScrollView(
//         child: Column(
//           children: [
//             SafeArea(
//               child: Padding(
//                 padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
//                 child: Row(
//                   children: [
//                     CustomIcon(
//                       icon: const Icon(
//                         Icons.chevron_left,
//                       ),
//                       onTap: () {
//                         Navigator.of(context).pop();
//                       },
//                       backgroundColor: Colors.white,
//                       borderColor: Colors.transparent,
//                       radius: 0,
//                     )
//                   ],
//                 ),
//               ),
//             ),
//             FutureBuilder<List<Food>>(
//               future: widget.ls,
//               builder: (context, snapshot) {
//                 if (snapshot.hasError) {
//                   return const Center(
//                     child: Text('Error'),
//                   );
//                 } else if (snapshot.hasData) {
//                   return ListView.builder(
//                     shrinkWrap: true,
//                     primary: false,
//                     itemCount: snapshot.data!.length,
//                     itemBuilder: (context, index) {
//                       var f = snapshot.data![index];
//                       return FoodCard(food: f);
//                     },
//                   );
//                 }
//                 return const Center(
//                   child: CircularProgressIndicator(),
//                 );
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
