// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cafe/presentation/widget/import.widget.dart';
import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';

import '../../../domain/entities/food.dart';

class FoodDetailPage extends StatefulWidget {
  final Food food;
  const FoodDetailPage({
    Key? key,
    required this.food,
  }) : super(key: key);

  @override
  State<FoodDetailPage> createState() => _FoodDetailPageState();
}

class _FoodDetailPageState extends State<FoodDetailPage> {
  int order = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: <Widget>[
        _img(context),
        _foodInfo(context),
        _header(context),
        _orderButton(context),
      ]),
    );
  }

  Widget _header(context) => SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CustomIcon(
                icon: const Icon(Icons.chevron_left),
                onTap: () {
                  Navigator.of(context).pop();
                },
                backgroundColor: Colors.white,
                radius: 10,
              ),
              CustomIcon(
                icon: const Icon(
                  Icons.favorite,
                  color: Colors.red,
                ),
                onTap: () {
                  print('Heart');
                },
                backgroundColor: Colors.white60,
              ),
            ],
          ),
        ),
      );

  Widget _img(context) => SizedBox(
        height: MediaQuery.of(context).size.height / 2,
        child: Image.network(
          widget.food.image,
          fit: BoxFit.cover,
        ),
      );

  Widget _foodInfo(context) => SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height / 2 - 16,
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SingleChildScrollView(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    CustomText(
                        text: widget.food.name,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                    CustomText(
                        text: '៛ ${widget.food.price}',
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ],
                ),
              ),
              // Container(
              //   margin: const EdgeInsets.only(top: 5),
              //   child: CustomText(
              //     text: widget.food.categoryId.toString(),
              //   ),
              // ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      ...List.generate(
                          5,
                          (index) => Icon(
                                (index != 4) ? Icons.star : Icons.star_half,
                                color: Colors.amber,
                              )),
                      const SizedBox(
                        width: 10,
                      ),
                      const CustomText(
                        text: '4.5 (1k+1)',
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      CustomIcon(
                        icon: const Icon(Icons.remove, size: 18),
                        onTap: () {
                          if (order != 0) {
                            setState(() {
                              --order;
                            });
                          }
                        },
                        backgroundColor: Colors.grey.shade300,
                        borderColor: Colors.white,
                        radius: 5,
                      ),
                      const SizedBox(width: 10),
                      CustomText(
                        text: order.toString(),
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                      const SizedBox(width: 10),
                      CustomIcon(
                        icon: const Icon(Icons.add, size: 18),
                        onTap: () {
                          setState(() {
                            ++order;
                          });
                        },
                        backgroundColor: Colors.grey.shade300,
                        borderColor: Colors.white,
                        radius: 5,
                      )
                    ],
                  ),
                ],
              ),
              _description(description: widget.food.description),
            ],
          ),
        ),
      );
  Widget _description({required String description}) => Container(
        margin: const EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const CustomText(
              text: 'Decription',
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            const SizedBox(
              height: 20,
            ),
            ReadMoreText(
              description,
              trimCollapsedText: 'Read More',
              trimExpandedText: 'Read Less',
              trimLines: 3,
              trimMode: TrimMode.Line,
              style: const TextStyle(fontSize: 18),
            ),
          ],
        ),
      );

  _orderButton(BuildContext context) => Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          padding: const EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomIcon(
                icon: const Icon(
                  Icons.shopping_bag_outlined,
                  size: 40,
                  color: Colors.green,
                ),
                backgroundColor: Colors.white,
                radius: 10,
                onTap: () {},
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: Colors.green,
                    shadowColor: Colors.white,
                    elevation: 1,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    minimumSize:
                        Size(MediaQuery.of(context).size.width * 0.7, 50),
                  ),
                  child: const CustomText(
                    text: 'Place an Order',
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
