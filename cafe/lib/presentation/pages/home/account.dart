import 'package:cafe/presentation/const/controller.dart';
import 'package:cafe/presentation/const/user.key.dart';
import 'package:cafe/presentation/pages/setting/profile.page.dart';
import 'package:cafe/presentation/util/import.util.dart';
import 'package:cafe/presentation/widget/custom.text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class Account extends StatelessWidget {
  //final AuthController controller = Get.find();
  const Account({Key? key}) : super(key: key);

  // String? img;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      const CircleAvatar(
                        backgroundImage: AssetImage('assets/images/avatar.jpg'),
                      ),
                      const SizedBox(width: 10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          CustomText(
                            text: GetStorage().read(name),
                            fontSize: 16,
                          ),
                          const CustomText(text: 'Edit User')
                        ],
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                        padding: const EdgeInsets.only(top: 15),
                        child: Text(GetStorage().read(email))),
                  ),
                ],
              ),
              const Divider(),
              Flexible(
                //flex: 3,
                child: GridView(
                  shrinkWrap: true,
                  primary: false,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisExtent: 80,
                    childAspectRatio: 32,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                  ),
                  padding: const EdgeInsets.all(10),
                  children: [
                    CustomSetting(
                      onTap: () {},
                      title: 'ការបញ្ចាទិញ',
                      icons: Icons.menu_book,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'ការវាយតម្លៃ',
                      icons: Icons.star,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'ការបញ្ចូលទឹកប្រាក់',
                      icons: Icons.account_circle,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'Return List',
                      icons: Icons.password,
                    ),
                    CustomSetting(
                      onTap: () {
                        //FirebaseAuth.instance.signOut();
                      },
                      title: 'Refund',
                      icons: Icons.logout_sharp,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'បញ្ជីសំណព្វចិត្ត',
                      icons: Icons.favorite,
                    ),
                    CustomSetting(
                      onTap: () {
                        //FirebaseAuth.instance.signOut();
                      },
                      title: 'ការទូទាត់',
                      icons: Icons.logout_sharp,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'កញ្ចប់ទំនិញ',
                      icons: Icons.password,
                    ),
                    CustomSetting(
                      onTap: () {
                        //FirebaseAuth.instance.signOut();
                      },
                      title: 'ប្ដូរពាក្យសម្ងាត់',
                      icons: Icons.logout_sharp,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'ប្ដូរអាសយដ្ឋាន',
                      icons: Icons.password,
                    ),
                    CustomSetting(
                      onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const ProfilePage(),
                      )),
                      title: 'កែប្រែគណនី',
                      icons: Icons.account_circle,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'ឧបករណ៏ផ្ទុក',
                      icons: Icons.password,
                    ),
                    CustomSetting(
                      onTap: () {
                        //FirebaseAuth.instance.signOut();
                      },
                      title: 'ភាសា',
                      icons: Icons.logout_sharp,
                    ),
                    CustomSetting(
                      onTap: () {},
                      title: 'បើកហាងលក់',
                      icons: Icons.password,
                    ),
                    CustomSetting(
                      onTap: () {
                        Get.defaultDialog(
                          title: 'SignOut',
                          confirm: ElevatedButton(
                              onPressed: authController.signOut,
                              child: const Text('Comfirm')),
                        );
                      },
                      title: 'ចាកចេញ',
                      icons: Icons.logout_sharp,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
