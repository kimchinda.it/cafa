// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cafe/presentation/const/user.key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../controller/home/home_controller.dart';
import '../../util/filtter.button.dart';

class HomePage extends StatelessWidget {
  final HomeController controller = Get.put(HomeController());
  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: ListView(
          children: <Widget>[
            _header,
            // SearchBar(
            //     hintText: 'ស្វែងរកឈ្មោះម្ហូប',
            //     ontap: () {
            //       showSearch(context: context, delegate: CustomDelegate());
            //     }),
            _button,
            // SizedBox(
            //   height: 200,
            //   child: controller.obx(
            //     (state) => ListView.builder(
            //       scrollDirection: Axis.horizontal,
            //       itemCount: controller.ls.length,
            //       itemBuilder: (context, index) {
            //         var food = controller.ls[index];
            //         return FiltterCard(food: food);
            //       },
            //     ),
            //   ),
            // ),
            ElevatedButton(
                onPressed: controller.streamData,
                child: Text(controller.ls.length.toString()))
            // _getCategory(context),
            //const CategoryPage(),
            //buildCategories,
          ],
        ),
      ),
      //drawer: const Drawer(),
    );
  }

  void getFood() async {}

  Widget get _header => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                  onPressed: () {
                    const Drawer();
                  },
                  icon: const Icon(Icons.menu)),
              CircleAvatar(
                radius: 16,
                backgroundColor: Colors.black,
                child: CircleAvatar(
                  radius: 15,
                  backgroundImage: NetworkImage(
                    GetStorage().read(image),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            'Hello Chinda',
            style: TextStyle(fontSize: 16, color: Colors.grey),
          ),
          const SizedBox(
            height: 5,
          ),
          const Text(
            'Food Delivery',
            style: TextStyle(
              fontSize: 25,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      );

  Widget _getCategory(context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          const Text(
            'Explore Category',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          GestureDetector(
            child: const Text(
              'View all',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: Colors.blue,
              ),
            ),
            onTap: () {
              // final con = FoodImplement(connection: Connection());
              // Navigator.of(context).push(
              //   MaterialPageRoute(
              //     builder: (context) => MenuFoodPage(
              //       ls: con.getFood(sort: 'desc'),
              //     ),
              //   ),
              // );
            },
          ),
        ],
      );

  Widget get _button => Container(
        height: 32,
        margin: const EdgeInsets.only(top: 16),
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: const <Widget>[
            FiltterButton(
              text: 'Popular',
              isSelected: true,
            ),
            FiltterButton(
              text: 'New items',
              isSelected: false,
            ),
            FiltterButton(
              text: 'Hot Details',
              isSelected: false,
            ),
            FiltterButton(
              text: 'Combos Picks',
              isSelected: false,
            ),
          ],
        ),
      );
}
