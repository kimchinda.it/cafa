import 'package:cafe/presentation/controller/home/main_controller.dart';
import 'package:flutter/material.dart';

import 'package:cafe/presentation/pages/home/account.dart';
import 'package:cafe/presentation/pages/home/categories.dart';
import 'package:get/get.dart';

import 'home.page.dart';

class MainPage extends StatelessWidget {
  final MainController controller = Get.put(MainController());
  MainPage({
    Key? key,
  }) : super(key: key);
  static final List<Widget> _page = [
    HomePage(),
    const FoodCategroy(),
    // OrderPage(),
    const Account(),
  ];
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        body: SafeArea(
          child: _page.elementAt(controller.selectedIndex.value),
        ),
        bottomNavigationBar: NavigationBarTheme(
          data: NavigationBarThemeData(
            indicatorColor: Colors.blue.shade100,
            labelTextStyle: MaterialStateProperty.all(
              const TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          child: NavigationBar(
            height: 50,
            backgroundColor: Colors.grey.shade200,
            selectedIndex: controller.selectedIndex.value,
            onDestinationSelected: controller.onDestinationSelected,
            destinations: const [
              NavigationDestination(
                  icon: Icon(Icons.home_outlined, color: Colors.redAccent),
                  selectedIcon: Icon(Icons.home, color: Colors.redAccent),
                  label: 'ទំព័រដើម'),
              NavigationDestination(
                icon: Icon(Icons.grid_view, color: Colors.redAccent),
                selectedIcon:
                    Icon(Icons.grid_view_rounded, color: Colors.redAccent),
                label: 'ប្រភេទ',
              ),
              NavigationDestination(
                icon: Icon(Icons.account_circle_outlined,
                    color: Colors.redAccent),
                selectedIcon:
                    Icon(Icons.account_circle, color: Colors.redAccent),
                label: 'គណនី',
              ),
            ],
          ),
        )));
  }
}
