// // ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'package:flutter/material.dart';
// import 'package:sodacafe/data/repositories/category_implement.dart';
// import 'package:sodacafe/domain/entities/category.dart';

// import 'package:sodacafe/presentation/widget/custom_card_category.dart';

// import '../../data/datasource/api_datasource.dart';

// class CategoryPage extends StatelessWidget {
//   const CategoryPage({
//     Key? key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final con = CategoryImplement(connection: Connection());
//     return FutureBuilder<List<Category>>(
//       future: con.getCategories(),
//       builder: (context, snapshot) {
//         if (snapshot.hasError) {
//           Center(child: Text('${snapshot.error}'));
//         } else if (snapshot.hasData) {
//           return GridView.builder(
//             shrinkWrap: true,
//             primary: false,
//             gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//               crossAxisCount: 2,
//               childAspectRatio: 16 / 6,
//             ),
//             itemCount: snapshot.data?.length,
//             itemBuilder: (context, index) {
//               var cat = snapshot.data![index];
//               return CustomCardCategory(
//                 category: cat,
//               );
//             },
//           );
//         }
//         return const Center(child: CircularProgressIndicator());
//       },
//     );
//   }
// }
