import 'dart:io';
import 'package:cafe/data/datasource/local_storage.dart';
import 'package:cafe/presentation/const/user.key.dart';
import 'package:cafe/presentation/util/import.util.dart';
import 'package:cafe/presentation/widget/import.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  File? file;
  final TextEditingController _conUserName =
      TextEditingController(text: LocalStorage.getString(key: name));
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(LocalStorage.getString(key: name) ?? '')),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: Column(
            children: [
              _imagePicker(context),
              const SizedBox(
                height: 20,
              ),
              CustomTextFormFiled(
                controller: _conUserName,
                labelText: 'User name',
              ),
              ElevatedButton(onPressed: () {}, child: const Text('Back'))
            ],
          ),
        ),
      ),
    );
  }

  Widget _imagePicker(context) => Center(
        child: Stack(
          children: <Widget>[
            CircleAvatar(
              radius: MediaQuery.of(context).size.width * 0.15,
              backgroundColor: Colors.black,
              child: CircleAvatar(
                radius: MediaQuery.of(context).size.width * 0.14,
                backgroundImage: (file != null) ? FileImage(file!) : null,
                backgroundColor: Colors.white60,
                child: (file != null)
                    ? null
                    : Icon(
                        Icons.add_photo_alternate,
                        size: MediaQuery.of(context).size.width * 0.12,
                        color: Colors.white,
                      ),
              ),
            ),
            Positioned(
              bottom: 9,
              right: 9,
              child: CustomIcon(
                  icon: Icon(
                    Icons.camera_alt,
                    color: Colors.white60,
                    size: MediaQuery.of(context).size.width * 0.05,
                  ),
                  onTap: () {
                    _showModalBottomSheet(context);
                  },
                  backgroundColor: Colors.white54),
            ),
          ],
        ),
      );

  void _showModalBottomSheet(context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      builder: (context) => DraggableScrollableSheet(
        expand: false,
        initialChildSize: 0.3,
        maxChildSize: 0.7,
        minChildSize: 0.3,
        builder: (context, scrollController) => SingleChildScrollView(
          controller: scrollController,
          child: OptionPickImage(onPressed: _pickerImage),
        ),
      ),
    );
  }

  Future _pickerImage({required ImageSource imageSource}) async {
    try {
      final image = await ImagePicker().pickImage(source: imageSource);
      if (image == null) return;
      final temp = File(image.path);
      final crop = await _cropImage(file: temp);
      setState(() {
        file = crop;
        Navigator.of(context).pop();
      });
    } on PlatformException {
      Navigator.of(context).pop();
    }
  }

  Future<File?> _cropImage({required File file}) async {
    CroppedFile? croppedFile =
        await ImageCropper().cropImage(sourcePath: file.path);
    if (croppedFile == null) return null;
    return File(croppedFile.path);
  }
}
