import 'package:cafe/presentation/pages/authenticate/sign.in.page.dart';
import 'package:cafe/presentation/pages/authenticate/sign.up.page.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

import '../../controller/import_auth_controller.dart';

class Authentication extends StatelessWidget {
  final AuthenticationController _controller =
      Get.put(AuthenticationController());

  Authentication({super.key});
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => _controller.isLoaded.value
          ? SignInPage(
              onClickedSignIn: _controller.onClicked,
            )
          : SignUpPage(
              onClickedSignIn: _controller.onClicked,
            ),
    );
  }
}
