// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:cafe/presentation/const/controller.dart';
import 'package:datetime_picker_formfield_new/datetime_picker_formfield_new.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

import 'package:cafe/presentation/widget/import.widget.dart';

import '../../themes/color_gradient.dart';

class SignUpPage extends StatelessWidget {
  //final SignUpController _controller = Get.put(SignUpController());
  final Function() onClickedSignIn;
  //final GlobalKey<FormState> _key = GlobalKey<FormState>();
  const SignUpPage({super.key, required this.onClickedSignIn});
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => authController.loading.value
          ? const Loading()
          : Scaffold(
              extendBodyBehindAppBar: true,
              appBar: AppBar(
                title: const CustomText(
                  text: 'Register',
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
                backgroundColor: Colors.transparent,
                foregroundColor: Colors.black,
                elevation: 0,
                centerTitle: true,
              ),
              body: Container(
                // height: MediaQuery.of(context).size.width * 1,
                padding: const EdgeInsets.only(top: 5, left: 20, right: 20),
                decoration: const BoxDecoration(gradient: linearGradient),
                child: ListView(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // _header,
                    _imagePicker(),
                    const SizedBox(height: 10),
                    Form(
                      key: authController.keySignUp,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CustomTextFormFiled(
                            controller: authController.conUserName,
                            labelText: 'User name',
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.name,
                            obscureText: false,
                            contentPadding: const EdgeInsets.only(left: 15),
                            autofillHints: const [AutofillHints.name],
                          ),
                          const SizedBox(height: 20),
                          //email
                          CustomTextFormFiled(
                            controller: authController.conEmail,
                            labelText: 'Email or phone',
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.emailAddress,
                            obscureText: false,
                            contentPadding: const EdgeInsets.only(left: 15),
                            autofillHints: const [AutofillHints.email],
                            validator: authController.emailValidator,
                          ),
                          const SizedBox(height: 20),
                          //password
                          CustomTextFormFiled(
                            controller: authController.conPass,
                            labelText: 'Password',
                            textInputAction: TextInputAction.next,
                            keyboardType: TextInputType.visiblePassword,
                            autofillHints: const [AutofillHints.password],
                            obscureText: authController.checkPassword.value,
                            contentPadding: const EdgeInsets.only(left: 15),
                            onChange: authController.onChangePassword,
                            suffixIcon: Visibility(
                              visible: authController.visiblePassword.value,
                              child: IconButton(
                                onPressed: authController.showPassword,
                                icon: authController.icon(),
                              ),
                            ),
                          ),
                          const SizedBox(height: 20),
                          //date of birth
                          DateTimeField(
                            controller: authController.conDatePicker,
                            format: DateFormat("dd/MM/yyyy"),
                            decoration: const InputDecoration(
                              labelText: 'Date of Birth',
                              // hintText: ',
                              hintStyle: TextStyle(
                                fontSize: 16,
                                color: Colors.black54,
                              ),
                              contentPadding: EdgeInsets.only(left: 15),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                            ),
                            onShowPicker: (context, currentValue) {
                              final today = DateTime.now();
                              return showDatePicker(
                                context: context,
                                firstDate: today.subtract(
                                  const Duration(days: 36500),
                                ),
                                initialDate: currentValue ?? DateTime.now(),
                                lastDate: today.add(
                                  const Duration(days: 36500),
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 20),
                          //gender
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.all(
                                Radius.circular(10),
                              ),
                              border: Border.all(color: Colors.black),
                            ),
                            padding: const EdgeInsets.only(left: 15, right: 10),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  size: 30,
                                ),
                                isExpanded: true,
                                value: authController.selectionGender.value,
                                items: authController.gender
                                    .map(bulidMenuItem)
                                    .toList(),
                                onChanged: authController.onChangeGender,
                              ),
                            ),
                          ),
                          const SizedBox(height: 20),
                          CustomButton(
                            title: 'Sign Up',
                            backgroundColor: Colors.pink,
                            radius: 15,
                            onPress: () => authController.signUp,
                          ),
                          const SizedBox(height: 20),
                          TextButton(
                            onPressed: onClickedSignIn,
                            child: const CustomText(
                              text: 'Already have an account?',
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget _imagePicker() => Center(
        child: Stack(
          children: <Widget>[
            authController.circleAvatar(),
            Positioned(
              bottom: 9,
              right: 9,
              child: CustomIcon(
                  icon: Icon(
                    Icons.camera_alt,
                    color: Colors.white60,
                    size: Get.size.width * 0.05,
                  ),
                  onTap: _bottomSheet,
                  backgroundColor: Colors.white54),
            ),
          ],
        ),
      );
  void _bottomSheet() => Get.bottomSheet(Container(
        color: Colors.white,
        child: Wrap(
          children: <ListTile>[
            ListTile(
              leading: const Icon(Icons.camera_enhance),
              title: const Text('Camera'),
              onTap: () =>
                  authController.pickerImage(imageSource: ImageSource.camera),
            ),
            ListTile(
              leading: const Icon(Icons.browse_gallery_sharp),
              title: const Text('Gallery'),
              onTap: () =>
                  authController.pickerImage(imageSource: ImageSource.gallery),
            )
          ],
        ),
      ));

  DropdownMenuItem<String> bulidMenuItem(String e) => DropdownMenuItem(
        value: e,
        child: Text(
          e,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 16,
          ),
        ),
      );
}
