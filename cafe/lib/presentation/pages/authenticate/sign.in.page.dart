// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:cafe/presentation/const/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../themes/color_gradient.dart';
import '../../widget/import.widget.dart';

class SignInPage extends StatelessWidget {
  //final AuthController controller = Get.put(AuthController());
  //final AuthService _auth = AuthService();
  final Function() onClickedSignIn;
  const SignInPage({Key? key, required this.onClickedSignIn}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => authController.loading.value
          ? const Loading()
          : Scaffold(
              body: Container(
                decoration: const BoxDecoration(
                  gradient: linearGradient,
                ),
                padding: const EdgeInsets.only(
                  top: 30,
                  left: 20,
                  right: 20,
                ),
                child: Form(
                  key: authController.keySignIn,
                  child: ListView(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const Center(
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                      CustomTextFormFiled(
                        controller: authController.conEmail,
                        labelText: 'Email or phone',
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        autofillHints: const [AutofillHints.email],
                        obscureText: false,
                        color: Colors.white,
                        contentPadding: const EdgeInsets.only(left: 15),
                        fontSize: 18,
                        validator: authController.emailValidator,
                      ),
                      const SizedBox(height: 20),
                      //password
                      CustomTextFormFiled(
                        controller: authController.conPass,
                        labelText: 'Password',
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.visiblePassword,
                        autofillHints: const [AutofillHints.password],
                        obscureText: authController.checkPassword.value,
                        contentPadding: const EdgeInsets.only(left: 15),
                        color: Colors.white,
                        fontSize: 18,
                        onChange: authController.onChangePassword,
                        suffixIcon: Visibility(
                          visible: authController.visiblePassword.value,
                          child: IconButton(
                            onPressed: authController.showPassword,
                            icon: authController.icon(),
                          ),
                        ),
                      ),
                      //password
                      const SizedBox(height: 20),
                      CustomButton(
                        title: 'Sign In',
                        backgroundColor: Colors.pink,
                        radius: 15,
                        onPress: authController.signIn,
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Text(
                            'Can\'t login?',
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                          TextButton(
                            onPressed: () {},
                            child: const Text(
                              'Forgot your password',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          Flexible(
                            flex: 1,
                            child: Divider(
                              color: Colors.black,
                              height: 10,
                              thickness: 2,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text('OR'),
                          SizedBox(
                            width: 5,
                          ),
                          Flexible(
                            flex: 1,
                            child: Divider(
                              color: Colors.black,
                              height: 10,
                              thickness: 2,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      CustomButton(
                        title: 'Sign Up',
                        radius: 15,
                        onPress: onClickedSignIn,
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
