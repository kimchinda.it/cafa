import 'package:cafe/presentation/widget/custom.text.dart';
import 'package:flutter/material.dart';

class Alert extends StatelessWidget {
  final String title;
  final String content;
  final Color colorTitle;
  final Color colorContent;
  final Color backgroundColor;
  final void Function()? onPreesed;

  const Alert({
    super.key,
    required this.title,
    required this.content,
    required this.onPreesed,
    this.backgroundColor = Colors.black87,
    this.colorContent = Colors.white,
    this.colorTitle = Colors.red,
  });
  @override
  Widget build(BuildContext context) => AlertDialog(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomText(
              text: title,
              color: colorTitle,
              fontSize: 16,
            ),
          ],
        ),
        content: CustomText(
          text: content,
          color: colorContent,
          fontSize: 16,
        ),
        backgroundColor: backgroundColor,
        actions: [
          ElevatedButton(
            onPressed: onPreesed,
            child: const Text('close'),
          ),
        ],
      );
}
