import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final void Function()? onPress;
  final double width;
  final double radius;
  final double fontSize;
  final FontWeight fontWeight;
  final Color foregroundColor;
  final Color backgroundColor;

  const CustomButton({
    super.key,
    required this.title,
    required this.onPress,
    this.width = 400,
    this.radius = 20,
    this.fontSize = 18,
    this.fontWeight = FontWeight.bold,
    this.foregroundColor = Colors.white,
    this.backgroundColor = Colors.blueAccent,
  });
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        foregroundColor: foregroundColor,
        backgroundColor: backgroundColor,
        shadowColor: Colors.white,
        elevation: 1,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
        minimumSize: Size(width, 50),
      ),
      onPressed: onPress,
      child: Text(
        title,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
        ),
      ),
    );
  }
}
