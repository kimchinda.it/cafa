// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:flutter/material.dart';

class CustomIcon extends StatelessWidget {
  final Widget icon;
  final void Function() onTap;
  final Color backgroundColor;
  final Color borderColor;
  final double radius;
  const CustomIcon({
    Key? key,
    required this.icon,
    required this.onTap,
    required this.backgroundColor,
    this.radius = 40,
    this.borderColor = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: backgroundColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
          side: BorderSide(width: 0.1, color: borderColor)),
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: icon,
        ),
      ),
    );
  }
}
