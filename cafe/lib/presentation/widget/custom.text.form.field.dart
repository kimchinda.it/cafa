import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class CustomTextFormFiled extends StatelessWidget {
  TextEditingController? controller;
  String? labelText;
  String? hintText;
  String? errorText;
  Color? color;
  TextInputAction? textInputAction;
  TextInputType? keyboardType;
  bool obscureText;
  double? fontSize;
  EdgeInsets? contentPadding;
  Widget? suffixIcon;
  String? Function(String? value)? validator;
  Iterable<String>? autofillHints;
  void Function(String value)? onChange;
  void Function()? onTap;
  List<TextInputFormatter>? inputFormatters;
  CustomTextFormFiled({
    super.key,
    this.controller,
    this.labelText,
    this.color = Colors.black54,
    this.errorText,
    this.textInputAction,
    this.keyboardType,
    this.obscureText = false,
    this.fontSize = 16,
    this.contentPadding,
    this.suffixIcon,
    this.validator,
    this.inputFormatters,
    this.autofillHints,
    this.onChange,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      textInputAction: textInputAction,
      keyboardType: keyboardType,
      obscureText: obscureText,
      inputFormatters: inputFormatters,
      style: _textStyle,
      decoration: InputDecoration(
        errorText: errorText,
        labelText: labelText,
        //labelStyle: const TextStyle(color: Colors.yellow),
        hintText: hintText,
        hintStyle: _textStyle,
        contentPadding: contentPadding,
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        suffixIcon: suffixIcon,
      ),
      autofillHints: autofillHints,
      validator: validator,
      onChanged: onChange,
      onTap: onTap,
      maxLines: 1,
    );
  }

  TextStyle get _textStyle => TextStyle(
        fontSize: fontSize,
        color: color,
      );
}
