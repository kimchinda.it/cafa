import 'dart:io';

import 'package:cafe/presentation/util/import.util.dart';
import 'package:flutter/material.dart';

import 'package:cafe/presentation/widget/import.widget.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart' as imgpicker;

// ignore: must_be_immutable
class ImgPicker extends StatefulWidget {
  File? file;
  ImgPicker(
    param0, {
    Key? key,
    this.file,
  }) : super(key: key);

  @override
  State<ImgPicker> createState() => _ImgPickerState();
}

class _ImgPickerState extends State<ImgPicker> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        children: <Widget>[
          CircleAvatar(
            radius: MediaQuery.of(context).size.width * 0.15,
            backgroundColor: Colors.black,
            child: CircleAvatar(
              radius: MediaQuery.of(context).size.width * 0.14,
              backgroundImage:
                  (widget.file != null) ? FileImage(widget.file!) : null,
              backgroundColor: Colors.white60,
              child: (widget.file != null)
                  ? null
                  : Icon(
                      Icons.add_photo_alternate,
                      size: MediaQuery.of(context).size.width * 0.12,
                      color: Colors.white,
                    ),
            ),
          ),
          Positioned(
            bottom: 9,
            right: 9,
            child: CustomIcon(
                icon: Icon(
                  Icons.camera_alt,
                  color: Colors.white60,
                  size: MediaQuery.of(context).size.width * 0.05,
                ),
                onTap: () {
                  _showModalBottomSheet(context);
                },
                backgroundColor: Colors.white54),
          ),
        ],
      ),
    );
  }

  void _showModalBottomSheet(context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(20),
        ),
      ),
      builder: (context) => DraggableScrollableSheet(
        expand: false,
        initialChildSize: 0.3,
        maxChildSize: 0.7,
        minChildSize: 0.3,
        builder: (context, scrollController) => SingleChildScrollView(
          controller: scrollController,
          child: OptionPickImage(onPressed: _pickerImage),
        ),
      ),
    );
  }

  Future _pickerImage({required imgpicker.ImageSource imageSource}) async {
    try {
      final image =
          await imgpicker.ImagePicker().pickImage(source: imageSource);
      if (image == null) return;
      final temp = File(image.path);
      final crop = await _cropImage(file: temp);
      setState(() {
        widget.file = crop;
        Navigator.of(context).pop();
      });
    } on PlatformException {
      Navigator.of(context).pop();
    }
  }

  Future<File?> _cropImage({required File file}) async {
    CroppedFile? croppedFile =
        await ImageCropper().cropImage(sourcePath: file.path);
    if (croppedFile == null) return null;
    return File(croppedFile.path);
  }
}
