import 'package:cafe/presentation/widget/custom.button.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class OptionPickImage extends StatelessWidget {
  final Function({required ImageSource imageSource}) onPressed;

  const OptionPickImage({super.key, required this.onPressed});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          CustomButton(
            title: 'Use a Gallery',
            backgroundColor: Colors.pink,
            width: MediaQuery.of(context).size.width * 0.8,
            radius: 15,
            onPress: () => onPressed(imageSource: ImageSource.gallery),
          ),
          const SizedBox(
            height: 10,
          ),
          const Center(
            child: Text('OR'),
          ),
          const SizedBox(
            height: 10,
          ),
          CustomButton(
            title: 'Use a Camera',
            backgroundColor: Colors.pink,
            width: MediaQuery.of(context).size.width * 0.8,
            radius: 15,
            onPress: () => onPressed(imageSource: ImageSource.camera),
          ),
        ],
      ),
    );
  }
}
