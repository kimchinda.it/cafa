import 'package:flutter/material.dart';

class FiltterButton extends StatelessWidget {
  final String text;
  final bool isSelected;
  const FiltterButton(
      {required this.text, required this.isSelected, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 10),
      child: OutlinedButton(
        onPressed: () {},
        style: OutlinedButton.styleFrom(
          backgroundColor:
              (isSelected == true) ? Colors.green : Colors.transparent,
          foregroundColor: (isSelected == true) ? Colors.white : Colors.green,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        child: Text(text),
      ),
    );
  }
}
