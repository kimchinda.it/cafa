// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cafe/presentation/widget/import.widget.dart';
import 'package:flutter/material.dart';

class CustomSetting extends StatelessWidget {
  final void Function() onTap;
  final String title;
  final IconData icons;
  const CustomSetting({
    Key? key,
    required this.onTap,
    required this.title,
    this.icons = Icons.star,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        //margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.only(left: 10),
        width: MediaQuery.of(context).size.width * 0.5,
        //height: 30,
        height: MediaQuery.of(context).size.width * 0.1,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 5),
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                color: Colors.grey.shade500,
                offset: const Offset(4, 4),
                blurRadius: 10,
                spreadRadius: 1),
            const BoxShadow(
                color: Colors.white,
                offset: Offset(-4, -4),
                blurRadius: 10,
                spreadRadius: 1)
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              icons,
              size: 40,
              color: Colors.redAccent,
            ),
            CustomText(
              text: title,
              fontWeight: FontWeight.bold,
            )
          ],
        ),
      ),
    );
  }
}
