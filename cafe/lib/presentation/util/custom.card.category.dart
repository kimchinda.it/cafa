// // ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'package:flutter/material.dart';
// import 'package:sodacafe/data/datasource/api_datasource.dart';
// import 'package:sodacafe/data/repositories/food_implement.dart';

// import 'package:sodacafe/domain/entities/category.dart';
// import 'package:sodacafe/presentation/pages/menu_food.dart';

// class CustomCardCategory extends StatelessWidget {
//   final Category category;
//   const CustomCardCategory({
//     Key? key,
//     required this.category,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return InkWell(
//       onTap: () {
//         final con = FoodImplement(connection: Connection());
//         Navigator.of(context).push(
//           MaterialPageRoute(
//             builder: (context) => MenuFoodPage(
//               ls: con.searchByCategoryId(categoryId: category.id),
//             ),
//           ),
//         );
//       },
//       child: Card(
//         child: Row(
//           children: [
//             Padding(
//               padding: const EdgeInsets.all(4),
//               child: ClipRRect(
//                 borderRadius: BorderRadius.circular(8),
//                 child: Image.network(
//                   category.img,
//                   width: 60,
//                   fit: BoxFit.cover,
//                   height: double.infinity,
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.all(8),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Text(category.name),
//                   Text(category.id.toString()),
//                 ],
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
