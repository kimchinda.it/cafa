// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cafe/presentation/pages/home/page.dart';
import 'package:cafe/presentation/widget/import.widget.dart';
import 'package:flutter/material.dart';
import '../../domain/entities/food.dart';

class FiltterCard extends StatelessWidget {
  final Food food;
  const FiltterCard({super.key, required this.food});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.6,
      margin: const EdgeInsets.only(right: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _img(context),
          _info,
        ],
      ),
    );
  }

  Widget _img(context) {
    return Stack(
      children: [
        InkWell(
          onTap: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => FoodDetailPage(food: food),
          )),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Image.network(
              food.image,
              height: 130,
              fit: BoxFit.cover,
              width: double.infinity,
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: CustomIcon(
              icon: const Icon(
                Icons.favorite_rounded,
                color: Colors.red,
              ),
              onTap: () {},
              backgroundColor: Colors.white),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Container(
            margin: const EdgeInsets.only(top: 10, left: 10),
            color: Colors.black45,
            width: 70,
            child: Row(children: <Widget>[
              const Icon(
                Icons.star,
                color: Colors.yellow,
                size: 20,
              ),
              _textStyle(
                  title: '4.5',
                  fontSize: 14,
                  fontWeight: FontWeight.normal,
                  color: Colors.white),
            ]),
          ),
        )
      ],
    );
  }

  Widget get _info => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CustomText(text: food.name),
          CustomText(text: food.size),
          CustomText(text: "៛ ${food.price}"),
        ],
      );
  Widget _textStyle({
    required String title,
    required double fontSize,
    required FontWeight fontWeight,
    Color? color = Colors.black,
  }) =>
      Text(
        title,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: color,
        ),
      );
}
