import 'package:flutter/material.dart';

class SearchBar extends StatelessWidget {
  final String hintText;
  final Icon icon;
  final void Function() ontap;
  const SearchBar({
    Key? key,
    required this.hintText,
    this.icon = const Icon(Icons.search),
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onTap: ontap,
      decoration: InputDecoration(
        hintText: hintText,
        suffixIcon: icon,
        contentPadding: const EdgeInsets.all(10),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
    );
  }
}
