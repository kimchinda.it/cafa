import 'package:flutter/material.dart';

class CustomDelegate extends SearchDelegate {
  //final _food = FoodImplement(connection: Connection());
  List<String> ls = [
    'ចៀនជួន',
    'ខត្រី',
    'ស្ងោរស៊ុបសាច់គោ',
    'សង់ខ្យាពង់មាន់',
  ];
  @override
  List<Widget>? buildActions(BuildContext context) => [
        IconButton(
            onPressed: () {
              if (query.isNotEmpty) {
                query = '';
              } else {
                close(context, null);
              }
            },
            icon: const Icon(Icons.clear)),
      ];
  @override
  Widget? buildLeading(BuildContext context) => IconButton(
      onPressed: () => close(context, null),
      icon: const Icon(Icons.arrow_back));

  @override
  Widget buildResults(BuildContext context) => Center(
        child: Text(query),
      );

  @override
  Widget buildSuggestions(BuildContext context) {
    List<String> l = ls.where((element) {
      final result = element.toLowerCase();
      final input = query.toLowerCase();
      return result.contains(input);
    }).toList();
    return ListView.builder(
      itemCount: l.length,
      itemBuilder: (context, index) {
        final s = l[index];
        return ListTile(
            leading: const Icon(Icons.history),
            title: Text(s),
            trailing: const Icon(Icons.clear),
            onTap: () {
              query = s;
              //buildResults(context);
              showResults(context);
            });
      },
    );
  }
}
