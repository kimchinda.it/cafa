// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cafe/presentation/widget/import.widget.dart';
import 'package:flutter/material.dart';

import '../../domain/entities/food.dart';

class FoodCard extends StatelessWidget {
  final Food food;
  const FoodCard({
    Key? key,
    required this.food,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Card(
          child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                food.image,
                width: 120,
                height: 120,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                CustomText(
                  text: food.name,
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
                CustomText(
                  text: food.size,
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
                CustomText(
                  text: '៛ ${food.price}',
                  fontSize: 20,
                  fontWeight: FontWeight.normal,
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
