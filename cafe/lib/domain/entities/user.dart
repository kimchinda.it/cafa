// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

List<User> userFromJson(String str) =>
    List<User>.from(json.decode(str).map((x) => User.fromJson(x)));

String userToJson(List<User> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  final String uid;
  final String name;
  final String email;
  final String gender;
  final String img;
  final String date;
  final int status;
  User({
    required this.uid,
    required this.name,
    required this.email,
    required this.gender,
    required this.img,
    required this.date,
    required this.status,
  });
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'uid': uid,
      'name': name,
      'email': email,
      'gender': gender,
      'img': img,
      'date': date,
      'status': status,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      uid: map['uid'] as String,
      name: map['name'] as String,
      email: map['email'] as String,
      gender: map['gender'] as String,
      img: map['img'] as String,
      date: map['date'] as String,
      status: map['status'] as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) =>
      User.fromMap(json.decode(source) as Map<String, dynamic>);
}
