import 'dart:convert';

import 'package:cafe/domain/entities/food.dart';

List<Category> categoryFromJson(String str) =>
    List<Category>.from(json.decode(str).map((x) => Category.fromJson(x)));

String categoryToJson(List<Category> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Category {
  Category({
    required this.id,
    required this.name,
    required this.img,
    required this.description,
    required this.foods,
  });

  final String id;
  final String name;
  final String img;
  final String description;
  final List<Food> foods;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        img: json["img"],
        description: json["description"],
        foods: json["foods"],
      );
  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "foods": foods,
      };
  @override
  String toString() {
    return "$id\t$name\t$description";
  }

  Category.empty({
    this.id = '',
    this.name = '',
    this.img = '',
    this.description = '',
    this.foods = const [],
  });
}
