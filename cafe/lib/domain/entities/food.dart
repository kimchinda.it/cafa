// ignore_for_file: public_member_api_docs, sort_constructors_first
// To parse this JSON data, do
//
//     final food = foodFromJson(jsonString);

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

List<Food> foodFromJson(String str) =>
    List<Food>.from(json.decode(str).map((x) => Food.fromJson(x)));
List<Food> searchFoodFromJson(String str) =>
    List<Food>.from(json.decode(str)['foods'].map((x) => Food.fromJson(x)));
String foodToJson(List<Food> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Food {
  Food({
    required this.uid,
    required this.name,
    required this.size,
    required this.price,
    required this.image,
    required this.description,
    required this.status,
    required this.postdate,
  });

  final String uid;
  final String name;
  final String size;
  final int price;
  final String image;
  final String description;
  final int status;
  final Timestamp postdate;

  factory Food.fromJson(Map<String, dynamic> json) => Food(
      uid: json["uid"],
      name: json["name"],
      size: json["size"],
      price: json["price"],
      image: json["image"],
      description: json["description"],
      status: json["status"],
      postdate: json["postdate"]);

  Map<String, dynamic> toJson() => {
        'uid': uid,
        "name": name,
        "size": size,
        "price": price,
        "image": image,
        "description": description,
        "status": status,
        "postdate": postdate,
      };
  // Food.emtpy({
  //   this.uid = '',
  //   this.name = '',
  //   this.size = '',
  //   this.price = 0,
  //   this.image = '',
  //   this.description = '',
  //   this.status = 0,
  //   this.postdate =,
  // });
}
