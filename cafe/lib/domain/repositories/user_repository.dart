import '../entities/user.dart';

abstract class UserRepository {
  Future<List<User>> getUser();
  Future<List<User>> searchUser({
    required String name,
    required String password,
  });
  Future<User> searchById({required int userId});
}
