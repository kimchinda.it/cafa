import '../entities/category.dart';

abstract class CategoryRepository {
  Future<List<Category>> getCategories();
  Future<Category> postCategory({required Category body});
  Future<Category> putCategory({required Category body});
  Future<bool> deleteCategory({required Category body});
  Future<Category> searchCategory({required int categoryId});
}
