import '../entities/food.dart';

abstract class FoodRepositories {
  Future<List<Food>> getFood({required String sort});
  Future<List<Food>> searchByCategoryId({required int categoryId});
  Future<Food> postFood({required Food body});
  Future<Food> putFood({required Food body});
  Future<bool> deleteFood({required Food body});
}
