// import 'dart:convert';
// import '../../domain/entities/food.dart';
// import '../../domain/repositories/food_repository.dart';
// import '../datasource/api_datasource.dart';

// class FoodImplement implements FoodRepositories {
//   final Connection _connection;
//   static const int _getStatusCode = 200;
//   static const int _postStatusCode = 201;
//   FoodImplement({required Connection connection}) : _connection = connection;

//   @override
//   Future<List<Food>> getFood({required String sort}) async {
//     try {
//       final response = await _connection.getFoods(sort: sort);
//       if (response.statusCode == _getStatusCode) {
//         return foodFromJson(response.body);
//       } else {
//         return const [];
//       }
//     } on Exception {
//       rethrow;
//     }
//   }

//   @override
//   Future<bool> deleteFood({required Food body}) async {
//     try {
//       final respone = await _connection.deleteFood(body: body);
//       if (respone.statusCode == _getStatusCode) {
//         return true;
//       } else {
//         return false;
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<Food> postFood({required Food body}) async {
//     try {
//       final respone = await _connection.postFood(body: body);
//       if (respone.statusCode == _postStatusCode) {
//         return Food.fromJson(json.decode(respone.body));
//       } else {
//         return Food.emtpy();
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<Food> putFood({required Food body}) async {
//     try {
//       final respone = await _connection.putFood(body: body);
//       if (respone.statusCode == _getStatusCode) {
//         return Food.fromJson(json.decode(respone.body));
//       } else {
//         return Food.emtpy();
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<List<Food>> searchByCategoryId({required int categoryId}) async {
//     try {
//       final respone =
//           await _connection.searchByCategoryId(categoryId: categoryId);
//       if (respone.statusCode == _getStatusCode) {
//         return searchFoodFromJson(respone.body);
//       } else {
//         return const [];
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }
// }
