// // ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'dart:convert';

// import '../../domain/entities/category.dart';
// import '../../domain/repositories/category_repository.dart';
// import '../datasource/api_datasource.dart';

// class CategoryImplement extends CategoryRepository {
//   final Connection _connection;
//   final int _getStatusCode = 200;
//   final int _postStatusCode = 201;
//   CategoryImplement({
//     required Connection connection,
//   }) : _connection = connection;

//   @override
//   Future<bool> deleteCategory({required Category body}) async {
//     try {
//       final response = await _connection.deleteCategory(body: body);
//       if (response.statusCode == _getStatusCode) {
//         return true;
//       } else {
//         return false;
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<List<Category>> getCategories() async {
//     try {
//       final respone = await _connection.getCategories();
//       if (respone.statusCode == _getStatusCode) {
//         return categoryFromJson(respone.body);
//       } else {
//         return const [];
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<Category> postCategory({required Category body}) async {
//     try {
//       final respone = await _connection.postCategory(body: body);
//       if (respone.statusCode == _postStatusCode) {
//         return Category.fromJson(json.decode(respone.body));
//       } else {
//         return Category.empty();
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<Category> putCategory({required Category body}) async {
//     try {
//       final respone = await _connection.putCategory(body: body);
//       if (respone.statusCode == _getStatusCode) {
//         return Category.fromJson(json.decode(respone.body));
//       } else {
//         return Category.empty();
//       }
//     } catch (e) {
//       rethrow;
//     }
//   }

//   @override
//   Future<Category> searchCategory({required int categoryId}) async {
//     try {
//       final respone = await _connection.searchCategory(categoryId: categoryId);
//       if (respone.statusCode == _getStatusCode) {
//         return Category.fromJson(json.decode(respone.body));
//       }
//       return Category.empty();
//     } catch (e) {
//       return Category.empty();
//     }
//   }
// }
