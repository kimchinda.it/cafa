import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  static late SharedPreferences _preferences;
  static Future init() async =>
      LocalStorage._preferences = await SharedPreferences.getInstance();
  static Future<bool> setString(
          {required String key, required String value}) async =>
      await _preferences.setString(key, value);
  static String? getString({required String key}) =>
      _preferences.getString(key);
  static Future<bool> setStringList(
          {required String key, required List<String> items}) async =>
      await _preferences.setStringList(key, items);
  static List<String>? getStringList({required String key}) =>
      _preferences.getStringList(key);
  static Future<bool> removeList({required String key}) async =>
      await _preferences.remove(key);
  static Future<bool> remove({required String key}) async =>
      await _preferences.remove(key);
}
